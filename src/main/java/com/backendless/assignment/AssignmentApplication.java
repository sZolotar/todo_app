package com.backendless.assignment;

import com.backendless.assignment.model.TodoObject;
import com.backendless.assignment.service.SortItemByPriority;
import com.backendless.assignment.service.TodoClientService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

@SpringBootApplication
@EnableScheduling
public class AssignmentApplication {

    private static TodoClientService client = new TodoClientService("TODO");
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(AssignmentApplication.class, args);

        System.out.println("");
        System.out.println("************ CREATE & SAVE ************");
        System.out.println("");

        /**
         * 1. create & save item
         */
        TodoObject item1 = new TodoObject("Task_1", "Add new item", 1);
        TodoObject item2 = new TodoObject("Task_2", "Get items", 2);
        TodoObject item3 = new TodoObject("Task_3", "Update item (set completed)", 3);
        TodoObject item4 = new TodoObject("Task_4", "Manual delete item", 5);
        TodoObject item5 = new TodoObject("Task_5", "Remove completed items (scheduler)", 4);

        System.out.println(format("Save item 1 - status: %d", client.postTodo(item1).getStatus()));
        System.out.println(format("Save item 2 - status: %d", client.postTodo(item2).getStatus()));
        System.out.println(format("Save item 3 - status: %d", client.postTodo(item3).getStatus()));
        System.out.println(format("Save item 4 - status: %d", client.postTodo(item4).getStatus()));
        System.out.println(format("Save item 5 - status: %d", client.postTodo(item5).getStatus()));

        Thread.sleep(2000);
        System.out.println("");
        System.out.println("************ GET ALL ************");
        System.out.println("");

        /**
         * Get all items
         */
        List<TodoObject> todoObjectList = client.getTodoItem();

        todoObjectList.sort(new SortItemByPriority()); // sort items by priority

        todoObjectList.forEach(System.out::println);

        Thread.sleep(2000);
        System.out.println("");
        System.out.println("************ GET BY ID ************");
        System.out.println("");

        /**
         * Get item by id
         */
        if (todoObjectList.size() > 0)
            System.out.println(client.getTodoItem(todoObjectList.get(todoObjectList.size() / 2).getObjectId()));

        Thread.sleep(2000);
        System.out.println("");
        System.out.println("************ UPDATE ITEM ************");
        System.out.println("");


        /**
         *  Update items set status completed for auto cleaner
         */
        for (int i = 0; i < todoObjectList.size() / 2; i++) {
            TodoObject object = todoObjectList.get(i);
            object.setCompleted(true);

            System.out.println(format("Item with id %s - update with status %s", object.getObjectId(),
                    client.updateItem(object.getObjectId(), object).getStatus()));
        }

        Thread.sleep(2000);
        System.out.println("");
        System.out.println("************ MANUAL DELETE ITEM ************");
        System.out.println("");

        /**
         * Manual remove last item
         */
        if (todoObjectList.size() > 0)
            System.out.println(client.deleteItem(todoObjectList.get(todoObjectList.size() - 1).getObjectId()).readEntity(String.class));

        System.out.println("");
        System.out.println("************ PLEASE WAIT FOR AUTO CLEANING (every 20 sec) ************");
        System.out.println("");
    }

    /**
     * Auto cleaner
     * <p>
     * Find all items mark as completed and delete from DB
     * <p>
     * Period = 20 sec
     */
    @Scheduled(fixedDelay = 20000)
    public void autoCleanupCompletedItems() {

        List<TodoObject> completedItems = client.getTodoItem()
                .stream()
                .filter(TodoObject::isCompleted)
                .collect(Collectors.toList());

        if (completedItems.size() > 0) {

            int count = 0;

            for (TodoObject todoObject : completedItems) {
                Response response = client.deleteItem(todoObject.getObjectId());
                if (response.getStatus() == 200)
                    count++;
            }

            System.out.println(format("%d item(s), has been removed by auto cleaner.", count));
        }
    }

}
