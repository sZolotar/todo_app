package com.backendless.assignment.service;

import com.backendless.assignment.model.ResponseError;
import com.backendless.assignment.model.TodoObject;

import javax.ws.rs.client.*;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

public class TodoClientService {

    private String HOST = "https://api.backendless.com/";
    private String APPLICATION_ID = "C3C3C553-3FBC-749E-FF36-17B8DD9CBF00";
    private String API_KEY = "210F38A3-3880-0CB0-FF89-E4FBA589DD00";

    private Client client;
    private WebTarget webTarget;

    public TodoClientService(String table) {

        client = ClientBuilder.newClient();
        webTarget = client.target(HOST + APPLICATION_ID + "/" + API_KEY + "/")
                .path("data/")
                .path(table);

    }


    public List<TodoObject> getTodoItem() {

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

        return invocationBuilder.get((new GenericType<List<TodoObject>>() {}));

    }

    public Object getTodoItem(String objectId) {

        WebTarget itemTarget = webTarget.path(objectId);
        Invocation.Builder invocationBuilder = itemTarget.request(MediaType.APPLICATION_JSON);

        Response response = invocationBuilder.get();

        if (response.getStatus() == 200) {
            return response.readEntity(TodoObject.class);
        } else
            return response.readEntity(ResponseError.class);

    }


    public Response postTodo(Object obj) {

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

        return invocationBuilder.post(Entity.entity(obj, MediaType.APPLICATION_JSON));

    }

    public Response updateItem(String objectId, Object obj) {

        WebTarget updateTarget = webTarget.path(objectId);

        Invocation.Builder invocationBuilder = updateTarget.request(MediaType.APPLICATION_JSON);

        return invocationBuilder.put(Entity.entity(obj, MediaType.APPLICATION_JSON));

    }

    public Response deleteItem(String objectId) {

        WebTarget deleteTarget = webTarget.path(objectId);

        Invocation.Builder invocationBuilder = deleteTarget.request(MediaType.APPLICATION_JSON);

        return invocationBuilder.delete();

    }

}

