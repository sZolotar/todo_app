package com.backendless.assignment.service;

import com.backendless.assignment.model.TodoObject;

import java.util.Comparator;

public class SortItemByPriority implements Comparator<TodoObject> {

    @Override
    public int compare(TodoObject o1, TodoObject o2) {
        return o1.getPriority().compareTo(o2.getPriority());
    }
}
