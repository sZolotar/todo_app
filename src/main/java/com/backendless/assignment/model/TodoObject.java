package com.backendless.assignment.model;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class TodoObject {

    @NonNull
    private String title;
    @NonNull
    private String task;
    @NonNull
    private Integer priority;
    private boolean completed;
    private Date deadline;

    private String objectId;
    private String ownerId;

    private Date created;
    private Date updated;

    private String ___class;
}

