package com.backendless.assignment.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ResponseError {

    private int code;
    private String message;
    private Object errorData;
}
