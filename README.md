TODO app with backendless REST api.

Build:
Use Maven for build project.

Run App:
All functional step-by-step is demonstrated in main() method AssignmentApplication.class
All request results is demonstrate in console and split to the blocks, separated comments (**** ... ****).

1. step 1 - create item and save it into DB use Backendless Data Service.
2. step 2 - get all items, sort by priority and print to console.
2. step 3 - get one item by objectID.
4. step 4 - update item (change status to completed).
5. step 5 - manual delete 1 item (last item).
6. step 6 - start auto cleaner every 20 sec, get all completed items and remove from DB.

Technology:
1. project - Spring Boot
2. Rest client - use Jersey
3. Timer - Spring Scheduler
